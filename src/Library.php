<?php
declare (strict_types=1);

namespace think\admin;

use stdClass;
use Psr\SimpleCache\CacheInterface;
use Hyperf\Contract\SessionInterface;
use function Hyperf\Support\make;
use think\admin\exception\HttpResponseException;

/**
 * 模块服务
 * @class Library
 * @package think\admin
 */
class Library
{
    /**
     * 设置数据缓存
     * @param string $name 缓存名称
     * @param mixed $value 缓存内容
     * @return boolean
     */
    public static function setCache(string $name, $value, $expire = 0)
    {
        return make(CacheInterface::class)->set($name, $value, $expire);
    }

    /**
     * 获取缓存数据
     * @param string $name 缓存名称
     * @return integer|string|array
     */
    public static function getCache(string $name, $default = '')
    {
        return make(CacheInterface::class)->get($name, $default);
    }

    /**
     * 删除缓存数据
     * @param string $name 缓存名称
     * @return boolean
     */
    public static function deleteCache(?string $name = null)
    {
        $cache = make(CacheInterface::class);
        return !$name ? $cache->clear() : $cache->delete($name);
    }

    /**
     * 设置数据session
     * @param string $name session名称
     * @param mixed $value session内容
     * @return boolean
     */
    public static function setSession(string $name, $value)
    {
        return make(SessionInterface::class)->set($name, $value);
    }
    
    /**
     * 获取sessionId
     * @return int
     */
    public static function getSessionId()
    {
        return make(SessionInterface::class)->getId();
    }

    /**
     * 获取session数据
     * @param string $name session名称
     * @return integer|string|array
     */
    public static function getSession(string $name, $default = '')
    {
        return make(SessionInterface::class)->get($name, $default);
    }

    /**
     * 删除session数据
     * @param string $name session名称
     * @return boolean
     */
    public static function deleteSession(?string $name = null)
    {
        $session = make(SessionInterface::class);
        return !$name ? $session->clear() : $session->forget($name);
    }
    
    /**
     * 返回格式化json
     * @param mixed $info 消息内容
     * @param mixed $data 返回数据
     * @param mixed $code 返回代码
     */
    public static function json($info = 'ok', $data = '{-null-}', $code = 1, $msgType = 'info')
    {
        if ($data === '{-null-}') $data = new stdClass();
        throw new HttpResponseException(is_string($info) ? lang($info) : $info, $data, $code, $msgType);
    }

    /**
     * 返回失败的内容
     * @param mixed $info 消息内容
     * @param mixed $data 返回数据
     * @param mixed $code 返回代码
     */
    public static function error($info, $data = '{-null-}', $code = 0, $msgType = 'info')
    {
        static::json($info, $data, $code, $msgType);
    }
    
    /**
     * 返回失败(msg)的内容
     * @param mixed $info 消息内容
     * @param mixed $data 返回数据
     * @param mixed $code 返回代码
     */
    public static function errorMsg($info, $data = '{-null-}', $code = 0)
    {
        return static::error($info, $data, $code, 'msg');
    }

    /**
     * 返回成功的内容
     * @param mixed $info 消息内容
     * @param mixed $data 返回数据
     * @param mixed $code 返回代码
     */
    public static function success($info, $data = '{-null-}', $code = 1, $msgType = 'info')
    {
        static::json($info, $data, $code, $msgType);
    }
    
    /**
     * 返回成功(msg)的内容
     * @param mixed $info 消息内容
     * @param mixed $data 返回数据
     * @param mixed $code 返回代码
     */
    public static function successMsg($info, $data = '{-null-}', $code = 0)
    {
        return static::success($info, $data, $code, 'msg');
    }
}