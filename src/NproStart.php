<?php
declare(strict_types=1);

namespace think\admin;

use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Framework\Bootstrap\ServerStartCallback;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class NproStart extends ServerStartCallback
{
    public function __construct(protected StdoutLoggerInterface $stdoutLogger)
    {
    }
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function beforeStart()
    {
        $this->stdoutLogger->info('Npro start success...');
        $this->stdoutLogger->info($this->welcome());
        str_contains(PHP_OS, 'CYGWIN') && $this->stdoutLogger->info('current booting the user: ' . shell_exec('whoami'));
    }

    protected function welcome(): string
    {
        $welcome = "
/----------------- welcome to use -----------------\
|    __    _   ______     _______        ____      |
|   |  \  | | |_   __ \  |_   __ \     .'    `.    |
|   |   \ | |   | |__) |   | |__) |   /  .--.  \   |
|   | |\ \| |   |  ___/    |  __ /    | |    | |   |
|   | | \   |  _| |_      _| |  \ \_  \  `--'  /   |
|   |_|  \__| |_____|    |____| |___|  `.____.'    |
|                                                  |
\__________  Copyright Npro 2024 ~ %y  __________/
            ";
        return str_replace(['%y'], [date('Y')], $welcome);
    }
}
