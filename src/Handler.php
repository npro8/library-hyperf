<?php
declare(strict_types=1);

namespace think\admin;

use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\Http\Message\ResponseInterface;
use think\admin\exception\HttpResponseException;
use think\admin\exception\SuccessException;
use think\admin\exception\ErrorException;
use Throwable;
use function Hyperf\Support\env;

class Handler extends ExceptionHandler
{
    /**
     * 响应结果数据.
     */
    protected $data = [];

    /**
     * Business code.
     * @var int
     */
    public $code = 0;

    /**
     * Business message.
     * @var string
     */
    public $message = '';

    /**
     * Business messageType.
     * @var string
     */
    public $messageType = 'info';
    
    /**
     * Business statusCode.
     * @var int
     */
    public $statusCode = 200;
    
    /**
     * Business error.
     * @var string
     */
    public $error = '';
    
    /**
     * Business errorMessage.
     * @var string
     */
    public $errorMessage = '';
    
    /**
     * Business responseData.
     * @var string
     */
    public $responseData = [];
    
    /**
     * Business header.
     * @var string
     */
    public $header = [];
    
    /**
     * 异常处理
     */
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        // 判断被捕获到的异常是希望被捕获的异常
        if ($throwable instanceof HttpResponseException || $throwable instanceof SuccessException || $throwable instanceof ErrorException) {
            if (isset($throwable->data)) $this->data = $throwable->data;
            if (isset($throwable->code)) $this->code = $throwable->code;
            if (isset($throwable->message)) $this->message = $throwable->message;
            if (isset($throwable->messageType)) $this->messageType = $throwable->messageType;
            if (isset($throwable->error)) $this->error = $throwable->error;
            if (isset($throwable->statusCode)) $this->statusCode = $throwable->statusCode;
            if (isset($throwable->header)) $this->header = $throwable->header;
            $this->stopPropagation(); $this->addDebugInfoToResponse($throwable);
            return $this->buildResponse($response);
        }

        // // 交给下一个异常处理器
        return $response;
    }
    
    /**
     * 判断该异常处理器是否要对该异常进行处理
     */
    public function isValid(Throwable $throwable): bool
    {
        return true;
    }

    /**
     * 调试模式：错误处理器会显示异常以及详细的函数调用栈和源代码行数来帮助调试，将返回详细的异常信息。
     * @param Throwable $throwable
     * @return void
     */
    protected function addDebugInfoToResponse(Throwable $throwable): void
    {
        if (!in_array($this->code, [0, 1, 2])) {
            $this->responseData['error'] = $this->error;
            $this->responseData['error_message'] = $this->errorMessage;
            $this->responseData['file'] = $throwable->getFile();
            $this->responseData['line'] = $throwable->getLine();
        }
    }

    /**
     * @inheritDoc
     */
    protected function buildResponse(ResponseInterface $response): ResponseInterface
    {
        $responseBody = [
            'code' => $this->code,
            $this->messageType => $this->message,
            'data' => $this->data
        ];
        
        if (!empty($this->responseData)) $responseBody = array_merge($responseBody, ['responseData' => $this->responseData]);
        
        $response = $response->withHeader('Content-Type', 'application/json;charset=utf-8');
        
        foreach ($this->header as $key => $value) $response->withHeader($key, $value);
        
        return $response->withStatus($this->statusCode)->withBody(new SwooleStream(json_encode($responseBody, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)));
    }
}