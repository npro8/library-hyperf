<?php
declare(strict_types=1);

namespace think\admin\exception;

use Hyperf\Server\Exception\ServerException;

class BaseException extends ServerException
{
    /**
     * HTTP Response Status Code.
     */
    public int $statusCode = 200;
    
    /**
     * Business Error message.
     * @var string
     */
    public string $errorMessage = 'The requested resource is not available or not exists';

    /**
     * HTTP Response Header.
     */
    public array $header = [];

    /**
     * Business data.
     * @var array|mixed
     */
    public $data = [];

    /**
     * Business code.
     * @var int
     */
    public int $httpCode = 0;

    /**
     * Business message.
     * @var string
     */
    public string $httpMessage = '';

    /**
     * Business messageType.
     * @var string
     */
    public string $messageType = 'info';

    /**
     * BaseException constructor.
     * @param string $errorMessage
     * @param ...
     */
    public function __construct(string $errorMessage = '', $data = [], int $httpCode = 1, string $msgType = 'info', array $header = [])
    {
        parent::__construct($errorMessage, $this->statusCode);
        $this->httpCode = $httpCode;
        $this->data = $data;
        $this->httpMessage = $errorMessage;
        $this->messageType = $msgType;
        $this->header = $header;
    }
}
