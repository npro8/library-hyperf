<?php
declare(strict_types=1);

namespace think\admin\exception;

class ErrorException extends BaseException
{
    /**
     * @var int
     */
    public int $statusCode = 200;

    /**
     * @var string
     */
    public string $errorMessage = 'Validation Error';
}
