<?php
declare (strict_types=1);

namespace think\admin;

use think\admin\extend\VirtualModel;
use Hyperf\Database\Query\Builder;
use Hyperf\DbConnection\Model\Model;
use think\admin\Request;
use function Hyperf\Support\make;

/**
 * 控制器助手
 * @class Helper
 * @package think\admin
 */
abstract class Helper
{
    /**
     * 请求参数对象
     */
    public $request;
    
    /**
     * 控制器实例
     * @var Controller
     */
    public $class;

    /**
     * 当前请求方式
     * @var string
     */
    public $method;

    /**
     * 自定输出格式
     * @var string
     */
    public $output;

    /**
     * Helper constructor.
     * @param Controller $class
     * @param Request $request
     */
    public function __construct(NproController $class, Request $request)
    {
        $this->class = $class; $this->request = $request;
        // 计算指定输出格式
        $output = $this->request->all()['output'] ?? 'default';
        $method = $this->request->getMethod() ?: 'cli';
        $this->output = strtolower("{$method}.{$output}");
    }

    /**
     * 实例对象反射
     * @param array $args
     * @return static
     */
    public static function instance(...$args): Helper
    {
        return make(static::class, $args);
    }

    /**
     * 获取数据库查询对象
     * @param Builder|Model|string $query
     * @return Query|Mongo|BaseQuery
     */
    public static function buildQuery($query)
    {
        if (is_string($query)) {
            return static::buildModel($query)->query();
        }
        if ($query instanceof Model) return $query->query();
        if ($query instanceof Builder) {
            $name = $query->getConnection()->getConfig('name') ?: '';
            if (is_string($name) && strlen($name) > 0) {
                $name = config("databases.{$name}") ? $name : '';
            }
            $query = static::buildModel($query->from, [], $name)->query();
        }
        return $query;
    }

    /**
     * 动态创建模型对象
     * @param mixed $name 模型名称
     * @param array $data 初始数据
     * @param mixed $conn 指定连接
     * @return Model
     */
    public static function buildModel(string $name, array $data = [], string $conn = ''): Model
    {
        if (strpos($name, '\\') !== false) {
            if (class_exists($name)) {
                $model = new $name($data);
                if ($model instanceof Model) return $model;
            }
            $name = basename(str_replace('\\', '/', $name));
        }
        return VirtualModel::mk($name, $data, $conn);
    }
}