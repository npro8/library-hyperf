<?php
declare (strict_types=1);

namespace think\admin;

use function Hyperf\Support\make;

/**
 * 自定义服务基类
 * @class Service
 * @package think\admin
 */
abstract class Service
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->initialize();
    }

    /**
     * 初始化服务
     */
    protected function initialize()
    {
    }

    /**
     * 静态实例对象
     * @param array $var 实例参数
     * @return static|mixed
     */
    public static function instance(array $var = [])
    {
        return make(static::class, $var);
    }
}