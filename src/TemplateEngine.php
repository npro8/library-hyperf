<?php
declare(strict_types=1);

namespace think\admin;

use Hyperf\View\Engine\EngineInterface;
use think\Template;

class TemplateEngine implements EngineInterface
{
    public function render($template, $data, $config): string
    {
        // 实例化对应的模板引擎的实例
        $engine = new Template($config);
        // 并调用对应的渲染方法
        return $engine->fetch($template, $data);
    }
}