<?php
declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 系统配置模型
 * @class SystemConfig
 * @package think\admin\model
 */
class SystemConfig extends Model
{
    /**
     * 表名
     * @var ?string
     */
    protected ?string $table = 'system_config';
}