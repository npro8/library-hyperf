<?php
declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 文件管理系统
 * @class SystemFile
 * @package think\admin\model
 */
class SystemFile extends Model
{
    /**
     * 表名
     * @var ?string
     */
    protected ?string $table = 'system_file';
    
    /**
     * 自动时间维护
     * @var bool
     */
    public bool $timestamps = true;
    
    /**
     * 自动时间字段
     */
    const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

    /**
     * 关联用户数据
     */
    public function user()
    {
        return $this->hasOne(SystemUser::class, 'id', 'uuid')->select(['id', 'username', 'nickname']);
    }

    /**
     * 格式化创建时间
     * @param mixed $value
     * @return string
     */
    public function getCreateAtAttribute($value): string
    {
        return format_datetime($value);
    }

    /**
     * 格式化更新时间
     * @param mixed $value
     * @return string
     */
    public function getUpdateAtAttribute($value): string
    {
        return format_datetime($value);
    }
}