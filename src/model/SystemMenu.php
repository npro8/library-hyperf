<?php
declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 系统菜单模型
 * @class SystemMenu
 * @package think\admin\model
 */
class SystemMenu extends Model
{
    /**
     * 表名
     * @var ?string
     */
    protected ?string $table = 'system_menu';
    
    /**
     * 日志名称
     * @var string
     */
    protected $oplogName = '系统菜单';

    /**
     * 日志类型
     * @var string
     */
    protected $oplogType = '系统菜单管理';

    /**
     * 格式化创建时间
     * @param mixed $value
     * @return string
     */
    public function getCreateAtAttribute($value): string
    {
        return format_datetime($value);
    }
}