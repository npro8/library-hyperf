<?php
declare (strict_types=1);

namespace think\admin\model;

use think\admin\Model;

/**
 * 授权节点模型
 * @class SystemNode
 * @package think\admin\model
 */
class SystemNode extends Model
{
    /**
     * 表名
     * @var ?string
     */
    protected ?string $table = 'system_auth_node';

    /**
     * 格式化创建时间
     * @param mixed $value
     * @return string
     */
    public function getCreateAtAttribute($value): string
    {
        return format_datetime($value);
    }
}