<?php
declare(strict_types=1);

namespace think\admin;

use Hyperf\Context\Context;
use Hyperf\HttpServer\Request as HyperfRequest;

class Request extends HyperfRequest
{
    /**
     * prop
     * @var array
     */
    protected $prop = [];
    
    public function __get($name)
    {
        return $name == 'prop' ? Context::get('prop') : (Context::get('prop')[$name]??Context::get($name));
    }

    public function __set($name, $value)
    {
        $this->prop[$name] = $value;
        Context::set('prop', $this->prop);
        return $this;
    }
    
    /**
     * 是否POST请求
     */
    public function isPost()
    {
        return $this->isMethod('post');
    }
    
    /**
     * 是否GET请求
     */
    public function isGet()
    {
        return $this->isMethod('get');
    }
    
    /**
     * GET参数
     */
    public function get($name = null, $default = '')
    {
        return $name ? $this->query($name, $default) : $this->query();
    }
    
    /**
     * 获取请求IP.
     */
    public function ip(): string
    {
        $ip = $this->getServerParams()['remote_addr'] ?? '0.0.0.0';
        $headers = $this->getHeaders();

        if (isset($headers['x-real-ip'])) {
            $ip = $headers['x-real-ip'][0];
        } elseif (isset($headers['x-forwarded-for'])) {
            $ip = $headers['x-forwarded-for'][0];
        } elseif (isset($headers['http_x_forwarded_for'])) {
            $ip = $headers['http_x_forwarded_for'][0];
        }

        return $ip;
    }

    /**
     * 获取协议架构.
     */
    public function getScheme(): string
    {
        if (isset($this->getHeader('X-scheme')[0])) {
            return $this->getHeader('X-scheme')[0] . '://';
        }
        return 'http://';
    }
}
