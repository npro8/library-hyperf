<?php
declare (strict_types=1);

namespace think\admin;

use stdClass;
use think\admin\traits\ControllerTrait;
use think\admin\service\NodeService;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Hyperf\View\RenderInterface;
use function Hyperf\Support\make;

/**
 * 标准控制器基类
 * @class Controller
 * @package think\admin
 */
class NproController extends stdClass
{
    /**
     * 基类方法
     */
    use ControllerTrait;
    
    /**
     * 当前节点
     */
    public $node = null;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->request = make(Request::class);
        $this->response = make(ResponseInterface::class);
        $this->view = make(RenderInterface::class);
        $this->node = NodeService::getCurrent();
        $this->initialize();
    }
    
    /**
     * 控制器初始化
     */
    protected function initialize()
    {
    }
}
