<?php
declare(strict_types=1);

namespace think\admin\handler;

use Hyperf\ExceptionHandler\ExceptionHandler;
use Swow\Psr7\Message\ResponsePlusInterface;
use Throwable;

class NotFoundHandler extends ExceptionHandler
{
    public function handle(Throwable $throwable, ResponsePlusInterface $response)
    {
        if (method_exists($throwable, 'getStatusCode') && $throwable->getStatusCode() == 404) {
            $this->stopPropagation();
            return $response->withStatus(404)->withContent(file_get_contents(BASE_PATH . '/public/404.html'));
        }
        return $response;
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }
}
