<?php
declare(strict_types=1);

namespace think\admin\handler;

use Hyperf\ExceptionHandler\ExceptionHandler;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class ErrorLogHandler extends ExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        error_file($throwable);
        return $response->withStatus(500)->withContent(file_get_contents(BASE_PATH . '/public/500.html'));
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }
}
