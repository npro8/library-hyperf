<?php
declare(strict_types=1);

namespace think\admin\helper;

use Hyperf\Paginator\Paginator;

/**
 * Bootstrap 分页驱动.
 */
class Bootstrap extends Paginator
{
    /**
     * 简易模式
     * bool $simple
     */
    protected $simple = false;

    /**
     * array $data
     */
    protected $data;

    /**
     * 设置数据.
     *
     * @param string $text
     *
     * @return string
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * 设置简易模式.
     *
     * @param string $text
     *
     * @return string
     */
    public function setSimple($simple)
    {
        $this->simple = $simple;
        return $this;
    }
    
    /**
     * 上一页按钮.
     *
     * @param string $text
     *
     * @return string
     */
    protected function getPreviousButton(string $text = '&laquo;'): string
    {
        if ($this->data['current_page'] <= 1) {
            return $this->getDisabledTextWrapper($text);
        }

        $url = $this->url(
            $this->data['current_page'] - 1
        );

        return $this->getPageLinkWrapper($url, $text);
    }

    /**
     * 下一页按钮.
     *
     * @param string $text
     *
     * @return string
     */
    protected function getNextButton(string $text = '&raquo;'): string
    {
        if (!$this->data['next_page_url']) {
            return $this->getDisabledTextWrapper($text);
        }

        $url = $this->url($this->data['current_page'] + 1);

        return $this->getPageLinkWrapper($url, $text);
    }

    /**
     * 页码按钮.
     *
     * @return string
     */
    protected function getLinks(): string
    {
        if ($this->simple) {
            return '';
        }

        $block = [
            'first'  => null,
            'slider' => null,
            'last'   => null,
        ];

        $side = 1;
        $window = $side * 3;

        if ($this->data['last_page'] < $window + 6) {
            $block['first'] = $this->getUrlRange(1, $this->data['last_page']);
        } elseif ($this->data['current_page'] <= $window) {
            $block['first'] = $this->getUrlRange(1, $window + 1);
            $block['last'] = $this->getUrlRange($this->data['last_page'] - 1, $this->data['last_page']);
        } elseif ($this->data['current_page'] > ($this->data['last_page'] - $window)) {
            $block['first'] = $this->getUrlRange(1, 2);
            $block['last'] = $this->getUrlRange($this->data['last_page'] - ($window), $this->data['last_page']);
        } else {
            $block['first'] = $this->getUrlRange(1, 1);
            $block['slider'] = $this->getUrlRange($this->data['current_page'], $this->data['current_page'] + $side);
            $block['last'] = $this->getUrlRange($this->data['last_page'], $this->data['last_page']);
        }

        $html = '';

        if (is_array($block['first'])) {
            $html .= $this->getUrlLinks($block['first']);
        }

        if (is_array($block['slider'])) {
            $html .= $this->getDots();
            $html .= $this->getUrlLinks($block['slider']);
        }

        if (is_array($block['last'])) {
            $html .= $this->getDots();
            $html .= $this->getUrlLinks($block['last']);
        }

        return $html;
    }

    /**
     * 渲染分页html.
     *
     * @return mixed
     */
    public function renders()
    {
        if ($this->data['last_page'] > 1) {
            if ($this->simple) {
                return sprintf(
                    '<ul class="pager">%s %s</ul>',
                    $this->getPreviousButton(),
                    $this->getNextButton()
                );
            } else {
                return sprintf(
                    '<ul class="pagination">%s %s %s</ul>',
                    $this->getPreviousButton(),
                    $this->getLinks(),
                    $this->getNextButton()
                );
            }
        }
    }

    /**
     * 生成一个可点击的按钮.
     *
     * @param string $url
     * @param string $page
     *
     * @return string
     */
    protected function getAvailablePageWrapper(string $url, string $page): string
    {
        return '<li><a href="' . htmlentities($url) . '">' . $page . '</a></li>';
    }

    /**
     * 生成一个禁用的按钮.
     *
     * @param string $text
     *
     * @return string
     */
    protected function getDisabledTextWrapper(string $text): string
    {
        return '<li class="disabled"><span>' . $text . '</span></li>';
    }

    /**
     * 生成一个激活的按钮.
     *
     * @param string $text
     *
     * @return string
     */
    protected function getActivePageWrapper(string $text): string
    {
        return '<li class="active"><span>' . $text . '</span></li>';
    }

    /**
     * 生成省略号按钮.
     *
     * @return string
     */
    protected function getDots(): string
    {
        return $this->getDisabledTextWrapper('...');
    }

    /**
     * 批量生成页码按钮.
     *
     * @param array $urls
     *
     * @return string
     */
    protected function getUrlLinks(array $urls): string
    {
        $html = '';

        foreach ($urls as $page => $url) {
            $html .= $this->getPageLinkWrapper($url, $page);
        }

        return $html;
    }

    /**
     * 生成普通页码按钮.
     *
     * @param string $url
     * @param string $page
     *
     * @return string
     */
    protected function getPageLinkWrapper(string $url, $page): string
    {
        $page = (string) $page;
        if ($this->data['current_page'] == $page) {
            return $this->getActivePageWrapper($page);
        }

        return $this->getAvailablePageWrapper($url, $page);
    }
}
