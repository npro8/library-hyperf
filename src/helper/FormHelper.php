<?php
declare (strict_types=1);

namespace think\admin\helper;

use think\admin\Helper;
use think\admin\service\SystemService;
use Hyperf\Database\Query\Builder;
use Hyperf\DbConnection\Model\Model;

/**
 * 表单视图管理器
 * @class FormHelper
 * @package think\admin\helper
 */
class FormHelper extends Helper
{

    /**
     * 逻辑器初始化
     * @param Builder|Model|string $dbQuery
     * @param string $template 视图模板名称
     * @param string $field 指定数据主键
     * @param mixed $where 限定更新条件
     * @param array $edata 表单扩展数据
     * @return void|array|boolean
     * @throws \think\admin\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function init($dbQuery, string $template = '', string $field = '', $where = [], array $edata = [])
    {
        $query = static::buildQuery($dbQuery); $request = $this->request;
        $field = $field ?: ($query->getModel()->getKeyName() ?: 'id');
        $value = $edata[$field] ?? $request->input($field);
        if ($request->isGet()) {
            if ($value !== null) {
                $exist = $query->where([$field => $value])->where($where)->first();
                if ($exist) $exist = $exist->toArray();
                $edata = array_merge($edata, $exist ?: []);
            }
            $prop = [];
            if (is_array($this->request->prop)) foreach ($this->request->prop as $k => $v) $prop[$k] = $v; // 前置前上下文
            if (false !== $this->class->callback('_form_filter', $edata)) {
                if (is_array($this->request->prop)) foreach ($this->request->prop as $k => $v) $prop[$k] = $v; // 前置后上下文拼接
                return $this->class->fetch($template, array_merge(['vo' => $edata], $prop));
            } else {
                return $edata;
            }
        }
        if ($request->isPost()) {
            $edata = array_merge($request->post(), $edata);
            if (false !== $this->class->callback('_form_filter', $edata, $where)) {
                $result = SystemService::save($query, $edata, $field, $where) !== false;
                if (false !== $this->class->callback('_form_result', $result, $edata)) {
                    if ($result !== false) {
                        $this->class->success('数据保存成功！');
                    } else {
                        $this->class->error('数据保存失败！');
                    }
                }
                return $result;
            }
        }
    }
}