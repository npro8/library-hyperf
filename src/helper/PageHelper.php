<?php
declare (strict_types=1);

namespace think\admin\helper;

use think\admin\Helper;
use think\admin\service\AdminService;
use Hyperf\Database\Query\Builder;
use Hyperf\DbConnection\Model\Model;
use Hyperf\HttpMessage\Cookie\Cookie;
use Hyperf\Database\Schema\Schema;
use Hyperf\Paginator\Paginator;

/**
 * 列表处理管理器
 * @class PageHelper
 * @package think\admin\helper
 */
class PageHelper extends Helper
{   
    /**
     * 逻辑器初始化
     * @param Builder|Model|string $dbQuery
     * @param boolean|integer $page 是否分页或指定分页
     * @param boolean $display 是否渲染模板
     * @param boolean|integer $total 集合分页记录数
     * @param integer $limit 集合每页记录数
     * @param string $template 模板文件名称
     * @return array
     */
    public function init($dbQuery, $page = true, bool $display = true, $total = false, int $limit = 0, string $template = '')
    {
        [$query, $cookie] = [$this->autoSortQuery($dbQuery), null];
        if ($page !== false) {
            $get = $this->request->get();
            $limits = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200];
            if ($limit <= 1) {
                $limit = $get['limit'] ?? $this->request->cookie('limit', 20);
                if (in_array($limit, $limits) && ($get['not_cache_limit'] ?? 0) < 1) {
                    $cookie = new Cookie('limit', ($limit = intval($limit >= 5 ? $limit : 20)) . '');
                    $this->class->response = $this->class->response->withCookie($cookie);
                }
            }
            $inner = strpos($get['spm'] ?? '', 'm-') === 0;
            $prefix = $inner ? (sysuri('admin/index/index') . '#') : '';
            // 生成分页数据
            $data = ($paginate = $query->paginate((int) $limit))->toArray();
            $result = ['page' => ['limit' => $data['per_page'], 'total' => $data['total'], 'pages' => $data['last_page'], 'current' => $data['current_page']], 'list' => $data['data']];
            // 分页跳转参数
            $select = "<select onchange='location.href=this.options[this.selectedIndex].value'>";
            if (in_array($limit, $limits)) foreach ($limits as $num) {
                $get = array_merge($get, ['limit' => $num, 'page' => 1]);
                $url = sysuri() . '?' . http_build_query($get, '', '&', PHP_QUERY_RFC3986);
                $select .= sprintf('<option data-num="%d" value="%s" %s>%d</option>', $num, $prefix . $url, $limit === $num ? 'selected' : '', $num);
            } else {
                $select .= "<option selected>{$limit}</option>";
            }
            $html = lang('共 %s 条记录，每页显示 %s 条，共 %s 页当前显示第 %s 页。', [$data['total'], "{$select}</select>", $data['last_page'], $data['current_page']]);
            $driver = config('npro.page_driver'); $config = ['path' => Paginator::resolveCurrentPath(), 'query' => $get];
            $render = (new $driver($paginate->items(), $paginate->perPage(), $paginate->currentPage(), $config))->setData($data)->renders();
            $link = $inner ? str_replace('<a href="', '<a data-open="' . $prefix, $render ?: '') : ($render ?: '');
            $this->class->assign('pagehtml', "<div class='pagination-container nowrap'><span>{$html}</span>{$link}</div>");
        } else {
            $result = ['list' => $query->get()->toArray()];
        }
        if (false !== $this->class->callback('_page_filter', $result['list'], $result) && $display) {
            if ($this->output === 'get.json') {
                $this->class->success('JSON-DATA', $result);
            } else {
                return $this->class->fetch($template, $result, $cookie);
            }
        }
        return $result;
    }

    /**
     * 组件 Layui.Table 处理
     * @param Builder|Model|string $dbQuery
     * @param string $template
     * @return array
     */
    public function layTable($dbQuery, string $template = '')
    {
        if ($this->output === 'get.json') {
            $get = $this->request->get();
            $query = static::buildQuery($dbQuery);
            // 根据参数排序
            if (isset($get['_field_']) && isset($get['_order_'])) {
                $dbQuery->orderByRaw("{$get['_field_']} {$get['_order_']}");
            }
            return PageHelper::instance()->init($query);
        }
        if ($this->output === 'get.layui.table') {
            $get = $this->request->get();
            $query = $this->autoSortQuery($dbQuery);
            // 根据参数排序
            if (isset($get['_field_']) && isset($get['_order_'])) {
                $query->orderByRaw("{$get['_field_']} {$get['_order_']}");
            }
            // 数据分页处理
            if (empty($get['page']) || empty($get['limit'])) {
                $data = $query->get()->toArray();
                $result = ['msg' => '', 'code' => 0, 'count' => count($data), 'data' => $data];
            } else {
                $data = $query->paginate((int) $get['limit'])->toArray();
                $result = ['msg' => '', 'code' => 0, 'count' => $data['total'], 'data' => $data['data']];
            }
            if (false !== $this->class->callback('_page_filter', $result['data'], $result)) {
                static::xssFilter($result['data']);
                return json($result);
            } else {
                return $result;
            }
        } else {
            return $this->class->fetch($template);
        }
    }

    /**
     * 输出 XSS 过滤处理
     * @param array $items
     */
    private static function xssFilter(array &$items)
    {
        foreach ($items as &$item) if (is_array($item)) {
            static::xssFilter($item);
        } elseif (is_string($item)) {
            $item = htmlspecialchars($item, ENT_QUOTES);
        }
    }

    /**
     * 绑定排序并返回操作对象
     * @param Model|string $dbQuery
     * @param string $field 指定排序字段
     */
    public function autoSortQuery($dbQuery, string $field = 'sort')
    {
        [$request, $query] = [$this->request, static::buildQuery($dbQuery)];
        if ($request->isPost() && $request->post('action') === 'sort') {
            AdminService::isLogin() or $this->class->error('请重新登录！');
            $model = $query->getModel();
            if (method_exists($model, 'getTable') && in_array($field, Schema::getColumnListing($model->getTable()))) {
                if ($request->post($pk = $model->getKeyName() ?: 'id', 'post')) {
                    $map = [$pk => $request->post($pk, 0)];
                    $data = [$field => intval($request->post($field, 0))];
                    $model->newQuery()->where($map)->update($data);
                    $this->class->success('列表排序成功！', '');
                }
            }
            $this->class->error('列表排序失败！');
        }
        return $query;
    }
}